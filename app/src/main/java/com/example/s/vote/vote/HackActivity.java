package com.example.s.vote.vote;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.s.vote.R;
import com.example.s.vote.vote.util.MemberItem;
import java.util.List;

public class HackActivity extends Activity implements HackInterface {
    HackPresenter presenter;
    RecyclerView recyclerView;
    MemberAdapter mAdapter;
    private List<MemberItem> items;
    private int pointsValue = 1;
    private Button saveBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.hack);

        presenter = new HackPresenter(this);

        saveBtn = findViewById(R.id.save_btn);
        recyclerView = findViewById(R.id.members_recycler);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        linearLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(linearLayoutManager);

        mAdapter = new MemberAdapter(items);
        recyclerView.setAdapter(mAdapter);
        presenter.getMembers();

        saveBtn.setOnClickListener(v->{
            presenter.setVotes();
        });


    }

    public class MemberAdapter extends RecyclerView.Adapter<MemberAdapter.ViewHolder> {
        private List<MemberItem> memberItems;

        public MemberAdapter(List<MemberItem> list) {
            this.memberItems = list;
        }


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.member_item, parent, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            final MemberItem item = memberItems.get(position);
            holder.membersName.setText(item.getMembersName());
            holder.cardMember.setOnClickListener(v->{
                startVote(holder.membersName.getText().toString());
            });
           // holder.pointsChangeAmount.setText((String.valueOf(pointsValue)));
           /* holder.btnMinus.setOnClickListener(v ->{
                saveBtn.setVisibility(View.VISIBLE);
                if (!holder.pointsChangeAmount.getText().toString().equals("")) {
                    int value = Math.max(1, Integer.parseInt(holder.pointsChangeAmount.getText().toString()) - 1);
                    holder.pointsChangeAmount.setText(String.valueOf(value));
                    presenter.setDone(holder.pointsChangeAmount.getText().toString(), holder.membersName.getText().toString());
                }
            });*/
          /*  holder.btnPlus.setOnClickListener(v ->{
                saveBtn.setVisibility(View.VISIBLE);
                int value = 0;
                String doneString = holder.pointsChangeAmount.getText().toString();
                int done = Integer.parseInt(doneString);
                int amount = 100;
                if (!doneString.equals("")) {
                    if (done == amount) {
                       alarmToastOne("Количество баллов максимальное");
                        return;
                    }
                    value = done + 1;
                    holder.pointsChangeAmount.setText(String.valueOf(value));

                    presenter.setDone(holder.pointsChangeAmount.getText().toString(), holder.membersName.getText().toString());
                }
            });*/
          //  holder.pointsChangeAmount.addTextChangedListener(new TextWatcher() {

             /*   @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    saveBtn.setVisibility(View.VISIBLE);
                    if(holder.pointsChangeAmount.getText().toString().length()!=0) {
                        if (Integer.valueOf(holder.pointsChangeAmount.getText().toString()) < 1) {
                            alarmToastOne("значение не может быть меньше единицы");
                            holder.pointsChangeAmount.setText(String.valueOf(pointsValue));
                        }
                        if (Integer.valueOf(holder.pointsChangeAmount.getText().toString()) > 100) {
                            alarmToastTwo();
                            holder.pointsChangeAmount.setText(String.valueOf(100));
                        }

                        presenter.setDone(holder.pointsChangeAmount.getText().toString(), holder.membersName.getText().toString());
                    }
                }
            });
*/
        }

        @Override
        public int getItemCount() {
            if (memberItems == null)
                return 0;
            return memberItems.size();
        }

        class ViewHolder extends RecyclerView.ViewHolder {
            private TextView membersName;
            private CardView cardMember;
            //private ImageButton btnMinus, btnPlus;
          //  private EditText pointsChangeAmount;




            public ViewHolder(View itemView) {
                super(itemView);
                membersName = itemView.findViewById(R.id.member_name);
                cardMember = itemView.findViewById(R.id.member_item);
               // btnMinus = findViewById(R.id.points_btn_minus);
              //  btnPlus = findViewById(R.id.points_btn_plus);
              //  pointsChangeAmount = findViewById(R.id.points_amount_change);

            }
        }

        public void dataChanged(List<MemberItem> list) {
            memberItems = list;
            notifyDataSetChanged();
        }
    }
    public void startVote(String member){
        Intent intent = new Intent(this, VoteActivity.class);
        intent.putExtra("S", member);
        startActivity(intent);
    }
    @Override
    public void paintRecycler(List<MemberItem> list) {
        mAdapter.dataChanged(list);

    }
    @Override
    public void alarmToastOne(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    @Override
    public void alarmToastTwo() {
        Toast.makeText(this, "значение не может превышать количество товара", Toast.LENGTH_LONG).show();
    }

    }

