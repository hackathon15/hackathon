package com.example.s.vote.select;

public interface SelectInterface {
    void navigateToHackActivity();
    void navigateToLectureActivity();
    void navigateToVideoActivity();
}
