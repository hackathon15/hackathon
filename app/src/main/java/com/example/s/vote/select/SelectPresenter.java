package com.example.s.vote.select;

public class SelectPresenter {
    SelectInterface view;
    SelectModel model;
    public SelectPresenter(SelectInterface activity) {
        this.view = activity;
        model = new SelectModel();
    }

    public void onHackBtnClick() {
        view.navigateToHackActivity();
    }


    public void onLectureBtnClick() {
        view.navigateToLectureActivity();
    }
    public void onVideoBtnClick() {
        view.navigateToVideoActivity();

    }
}
