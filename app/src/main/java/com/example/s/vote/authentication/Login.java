package com.example.s.vote.authentication;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.example.s.vote.R;
import com.example.s.vote.select.SelectActivity;
import com.wang.avi.AVLoadingIndicatorView;

public class Login extends Activity implements LoginInterface {

    LoginPresenter presenter;
    private AVLoadingIndicatorView progressBar;
    private FrameLayout frameLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        presenter = new LoginPresenter(this);
        progressBar = findViewById(R.id.progress_bar);
        frameLayout = findViewById(R.id.progress_bar_holder);

        final EditText login = findViewById(R.id.input_login);
        final EditText password = findViewById(R.id.input_password);

        Button btnLogin = findViewById(R.id.btn_login);
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (login.getText().toString().length() != 0 && password.getText().toString().length() != 0) {
                    presenter.logIn(login.getText().toString(), password.getText().toString());
                } else {
                    showToast();
                }
            }
        });
    }
    @Override
    public void showProgress() {
        progressBar.show();
        frameLayout.setVisibility(View.VISIBLE);
    }

    @Override
    public void hideProgress() {
        progressBar.hide();
        frameLayout.setVisibility(View.GONE);
    }

    @Override
    public void navigateToSelectActivity() {
        Intent intent = new Intent(this, SelectActivity.class);
        intent.putExtra("fromLogin", true);
        startActivity(intent);
    }

    void showToast() {
        Toast.makeText(this, "Введите логин и пароль", Toast.LENGTH_SHORT).show();
    }

    public void showFailedLoginToast() {
        Toast.makeText(this, "Не найдено совпадений логин/пароль", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void unknownError() {
        Toast.makeText(this, "Отсутствие интернет соединение, или внутренняя ошибка сервера, попробуйте еще раз",
                Toast.LENGTH_LONG).show();
    }

    @Override
    public void timeOut() {
        Toast.makeText(this, "Превышено ожидание выполнения запроса, попробуйте еще раз", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    }

