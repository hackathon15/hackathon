package com.example.s.vote.vote;

import android.app.Activity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.s.vote.R;

import org.w3c.dom.Text;

public class VoteActivity extends Activity {
    private ImageButton btnMinus, btnPlus;
     private EditText pointsChangeAmount;
     private int pointsValue = 1;
     private Button btnSave;
     private TextView text;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.vote);
        text = findViewById(R.id.text_huext);
        String member = getIntent().getExtras().getString("s");
        btnMinus = findViewById(R.id.points_btn_minus);
        btnPlus = findViewById(R.id.points_btn_plus);
        btnSave = findViewById(R.id.confirm);
        btnSave.setOnClickListener(v->{
            text.setVisibility(View.VISIBLE);
        });

        pointsChangeAmount = findViewById(R.id.points_amount_change);
        pointsChangeAmount.setText((String.valueOf(pointsValue)));
            btnMinus.setOnClickListener(v ->{
                if (pointsChangeAmount.getText().toString().equals("")) {
                    int value = Math.max(1, Integer.parseInt(pointsChangeAmount.getText().toString()) - 1);
                    pointsChangeAmount.setText(String.valueOf(value));
                    setDone(pointsChangeAmount.getText().toString(),member);
                }
            });
            btnPlus.setOnClickListener(v ->{

                int value = 0;
                String doneString = pointsChangeAmount.getText().toString();
                int done = Integer.parseInt(doneString);
                int amount = 100;
                if (!doneString.equals("")) {
                    if (done == amount) {
                       alarmToastOne("Количество баллов максимальное");
                        return;
                    }
                    value = done + 1;
                    pointsChangeAmount.setText(String.valueOf(value));

                    setDone(pointsChangeAmount.getText().toString(), member);
                }
            });
          pointsChangeAmount.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {

                }

                @Override
                public void afterTextChanged(Editable s) {
                    if(pointsChangeAmount.getText().toString().length()!=0) {
                        if (Integer.valueOf(pointsChangeAmount.getText().toString()) < 1) {
                            alarmToastOne("значение не может быть меньше единицы");
                            pointsChangeAmount.setText(String.valueOf(pointsValue));
                        }
                        if (Integer.valueOf(pointsChangeAmount.getText().toString()) > 100) {
                            alarmToastTwo();
                            pointsChangeAmount.setText(String.valueOf(100));
                        }

                        setDone(pointsChangeAmount.getText().toString(), member);
                    }
                }
            });
    }
    public void alarmToastOne(String s) {
        Toast.makeText(this, s, Toast.LENGTH_LONG).show();
    }

    public void alarmToastTwo() {
        Toast.makeText(this, "значение не может превышать количество товара", Toast.LENGTH_LONG).show();
    }
    public void setDone(String value, String member){

    }
}
