package com.example.s.vote.video;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.example.s.vote.R;
import com.example.s.vote.select.SelectInterface;

import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

public class VideoActivity extends Activity implements VideoInterface {
    VideoPresenter presenter;
    ListView listView ;
    ArrayAdapter<String> adapter;
    String[] lecture_keys;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.video_list);

        listView = (ListView) findViewById(R.id.list);
        SortedMap<String, String> lectures = new TreeMap<String, String>() {
        };
        lectures.put("lecture 01", "http://techslides.com/demos/sample-videos/small.mp4");
        lectures.put("lecture 02", "http://techslides.com/demos/sample-videos/small.webm");
        lectures.put("lecture 03", "http://techslides.com/demos/sample-videos/small.ogv");
        lectures.put("lecture 04", "http://techslides.com/demos/sample-videos/small.flv");
        lectures.put("lecture 05", "https://www.youtube.com/watch?v=HGmU9MCUtv4");
        lectures.put("lecture 06", "https://www.youtube.com/watch?v=HNbi8cpcoDw&t=8s");
        lectures.put("lecture 07", "https://www.youtube.com/watch?v=HGmU9MCUtv4");
        lectures.put("lecture 08", "https://www.youtube.com/watch?v=HNbi8cpcoDw&t=8s");
        lectures.put("lecture 09", "https://www.youtube.com/watch?v=HGmU9MCUtv4");
        lectures.put("lecture 10", "https://www.youtube.com/watch?v=HNbi8cpcoDw&t=8s");
        lectures.put("lecture 11", "https://www.youtube.com/watch?v=HGmU9MCUtv4");
        lectures.put("lecture 12", "https://www.youtube.com/watch?v=HNbi8cpcoDw&t=8s");

        lecture_keys = lectures.keySet().stream().toArray(String[]::new);
        adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, lecture_keys);

        listView.setAdapter(adapter);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

            @Override
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {

                // ListView Clicked item index
                String itemPosition = lecture_keys[position];

                // ListView Clicked item value
                String  itemValue    = lectures.get(itemPosition);

                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(itemValue));
                intent.setDataAndType(Uri.parse(itemValue), "video/*");
                startActivity(intent);

                // Show Alert
                /*Toast.makeText(getApplicationContext(),
                        "Position :"+itemPosition+"  ListItem : " +itemValue , Toast.LENGTH_LONG)
                        .show();
                        */

            }

        });


    }

    @Override
    public void onItemClick() {
    }
}
