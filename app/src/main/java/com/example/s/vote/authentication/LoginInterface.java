package com.example.s.vote.authentication;

public interface LoginInterface {
    void showProgress();
    void hideProgress();
    void navigateToSelectActivity();
    void showFailedLoginToast();
    void unknownError();
    void timeOut();
}
