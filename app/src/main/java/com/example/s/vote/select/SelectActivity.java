package com.example.s.vote.select;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.widget.Button;

import com.example.s.vote.R;
import com.example.s.vote.video.VideoActivity;
import com.example.s.vote.vote.HackActivity;

public class SelectActivity extends Activity implements SelectInterface {
    SelectPresenter presenter;
    CardView lectureCard, videoCard, hackCard, somethingCard;
    Button btnToLecture, btnToVideo, btnToHack, btnToSomething;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select);

        presenter = new SelectPresenter(this);

        lectureCard = findViewById(R.id.card_to_lecture);
        videoCard = findViewById(R.id.card_to_video);
        hackCard = findViewById(R.id.card_to_hackathon);
        somethingCard = findViewById(R.id.card_to_something);

        btnToLecture = findViewById(R.id.btn_to_lecture);
        btnToVideo = findViewById(R.id.btn_to_video);
        btnToHack = findViewById(R.id.btn_to_hackathon);
        btnToSomething = findViewById(R.id.btn_to_something);

        btnToHack.setOnClickListener(v->{
            presenter.onHackBtnClick();
        });
        btnToLecture.setOnClickListener(v->{
            presenter.onLectureBtnClick();
        });
        btnToVideo.setOnClickListener(v->{
            presenter.onVideoBtnClick();
        });

    }

    @Override
    public void navigateToHackActivity() {
        Intent intent = new Intent(this, HackActivity.class);
        startActivity(intent);
    }

    @Override
    public void navigateToLectureActivity() {
    }
    @Override
    public void navigateToVideoActivity() {
        Intent intent = new Intent(this, VideoActivity.class);
        startActivity(intent);
    }
}
