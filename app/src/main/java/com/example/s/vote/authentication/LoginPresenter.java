package com.example.s.vote.authentication;

public class LoginPresenter {
    LoginInterface view;
    LoginModel model;
    public LoginPresenter(LoginInterface activity) {
        this.view = activity;
        this.model = new LoginModel();
    }


    public void logIn(String login, String password) {
       // view.showProgress();
        model.logIn(login, password, this);
    }


    public void successLogin() {
        view.navigateToSelectActivity();
        view.hideProgress();
    }


    public void failedLogin() {
        view.hideProgress();
        view.showFailedLoginToast();
    }


    public void unknownError() {
        view.hideProgress();
        view.unknownError();
    }


    public void timeOut() {
        view.hideProgress();
        view.timeOut();
    }
}
