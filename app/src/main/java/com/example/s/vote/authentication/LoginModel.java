package com.example.s.vote.authentication;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.Query;
import com.google.firebase.database.annotations.NotNull;
import static android.content.ContentValues.TAG;



public class LoginModel {
    public void logIn(final String login, final String password, final LoginPresenter loginPresenter) {

       final Handler myHandler = new Handler();
        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Log.i("start "+ Thread.currentThread(), "Thread start");

                    FirebaseDatabase fb = FirebaseDatabase.getInstance();
                    DatabaseReference ref = fb.getReference().child("Users");
                    Query qu = ref.orderByChild("Login").equalTo(login);
                    qu.addChildEventListener(new ChildEventListener() {
                        @Override
                        public void onChildAdded(@NonNull @NotNull DataSnapshot dataSnapshot, @Nullable String s) {
                            String ss = dataSnapshot.child("Password").getValue(String.class);

                            if((ss.equals(password))){
                                loginPresenter.successLogin();
                                Log.d(TAG,"login OK"+ss);
                            } else {
                                loginPresenter.failedLogin();
                                Log.d(TAG,"login failed"+ss);
                            }
                        }

                        @Override
                        public void onChildChanged(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onChildRemoved(@NonNull DataSnapshot dataSnapshot) {

                        }

                        @Override
                        public void onChildMoved(@NonNull DataSnapshot dataSnapshot, @Nullable String s) {

                        }

                        @Override
                        public void onCancelled(@NonNull DatabaseError databaseError) {
                            loginPresenter.failedLogin();
                        }
                    });
                }
                catch (Exception ex) {myHandler.post(()->
                        loginPresenter.unknownError()
                ); Log.e(ex.getMessage(),"base loading error");}
            }}).start();

    }
}