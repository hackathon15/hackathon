package com.example.s.vote.video;

class VideoPresenter {
    VideoInterface view;
    VideoModel model;
    public VideoPresenter(VideoInterface activity) {
        this.view = activity;
        model = new VideoModel();
    }
}
