package com.example.s.vote.vote;

import com.example.s.vote.vote.util.MemberItem;

import java.util.List;

public interface HackInterface {
    void alarmToastOne(String s);
    void paintRecycler(List<MemberItem> list);
    void alarmToastTwo();
}
